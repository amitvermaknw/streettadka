import * as React from 'react';
import { StyleSheet, Image, ScrollView } from 'react-native';
import { Text, View } from '../components/Themed';
import { Card, Button, TextInput, Chip, Paragraph, Searchbar } from 'react-native-paper';
import { styles } from '../assets/css/CommanCss';


const AddTadkaScreen = (props: any) => {
    const [text, setText] = React.useState('');
    const [searchQuery, setSearchQuery] = React.useState('');
    const onChangeSearch = (query: any) => setSearchQuery(query);

    const UploadedImg = () => {
        return (
            <View style={[{ flexDirection: 'row', justifyContent: 'space-between' }, styles.marginT]}>
                <Image style={
                    pageStyles.vendorImg
                }
                    source={
                        { uri: 'https://picsum.photos/700' }
                    } />
                <Image style={
                    pageStyles.vendorImg
                }
                    source={
                        { uri: 'https://picsum.photos/700' }
                    } />
                <Image style={
                    pageStyles.vendorImg
                }
                    source={
                        { uri: 'https://picsum.photos/700' }
                    } />
                <Image style={
                    pageStyles.vendorImg
                }
                    source={
                        { uri: 'https://picsum.photos/700' }
                    } />
            </View>
        )
    };

    const FoodCategories = () => {
        return (
            <View style={[{ flexDirection: 'row' }, styles.marginT]}>
                <Chip
                    mode="outlined"
                    disabled={true}
                    style={styles.marginR}
                >
                    Samosa
            </Chip>
                <Chip
                    mode="outlined"
                    disabled={true}
                >
                    Dosa
            </Chip>
            </View>
        );
    };

    return (
        <View style={{ backgroundColor: "#eeeeee" }}>
            <ScrollView>
                <Card style={styles.card}>
                    <Card.Content>
                        <View>
                            <Searchbar
                                placeholder="Tadka name"
                                onChangeText={onChangeSearch}
                                value={searchQuery}
                            />
                            <FoodCategories />
                            <TextInput
                                label="Description"
                                value=""
                                multiline={true}
                                onChangeText={text => setText(text)}
                                mode="outlined"
                                style={styles.marginT}
                                numberOfLines={5}
                            />
                        </View>
                        <View>
                            <UploadedImg />
                        </View>
                        <View style={[{ flexDirection: 'row', justifyContent: 'space-between' }, styles.marginT]}>
                            <Button
                                icon="camera"
                                mode="outlined"
                                onPress={() => console.log('Pressed')}
                                style={{ padding: 5 }}

                            >
                                Add Image
                            </Button>
                        </View>
                        {/* <View style={{ marginTop: 40 }}>
                            <Button
                                mode="contained"
                                labelStyle={{ color: "white" }}
                                onPress={() => console.log('Pressed')}
                            >Save </Button>
                        </View> */}
                        <View style={[{ flexDirection: 'row-reverse' }, styles.marginT]}>
                            <View style={{ padding: 5 }}>
                                <Button
                                    mode="contained"
                                    labelStyle={{ color: "white" }}
                                    onPress={() => console.log('Pressed')}
                                    icon="check"
                                >Save </Button>
                            </View>
                            <View style={{ padding: 5 }}>
                                <Button
                                    mode="outlined"
                                    onPress={() => console.log('Pressed')}
                                    icon="close"
                                >Reset </Button>
                            </View>
                        </View>
                        {/* <View style={styles.marginT}>
                            <Button
                                mode="outlined"
                                onPress={() => console.log('Pressed')}
                            >Reset </Button>
                        </View> */}

                    </Card.Content>

                    {/* <Card.Actions>
                        <Button>Cancel</Button>
                        <Button mode="contained">Ok</Button>
                    </Card.Actions> */}

                </Card>

            </ScrollView>
        </View>
    );
}

export default AddTadkaScreen;

const pageStyles = StyleSheet.create({
    vendorImg: {
        width: 85,
        height: 85,
        borderRadius: 5,
        padding: 10

    }
});