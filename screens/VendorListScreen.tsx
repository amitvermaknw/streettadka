import * as React from 'react';
import {StyleSheet, TextInput, Image, ScrollView} from 'react-native';
import {Text, View} from '../components/Themed';
import {
    Avatar,
    Button,
    Card,
    Title,
    Paragraph
} from 'react-native-paper';
import { Rating } from 'react-native-ratings';
import { Chip } from 'react-native-paper';
import { styles } from '../assets/css/CommanCss';
import { TouchableRipple } from 'react-native-paper';

const LeftContent = (props : any) => <Avatar.Text size={45} label="ST"/>

const VendorImage = () => {

    return (
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <Image style={
                    pageStyles.vendorImg
                }
                source={
                    {uri: 'https://picsum.photos/700'}
                }/>
            <Image style={
                    pageStyles.vendorImg
                }
                source={
                    {uri: 'https://picsum.photos/700'}
                } />
            <Image style={
                    pageStyles.vendorImg
                }
                source={
                    {uri: 'https://picsum.photos/700'}
                } />
            <Image style={
                    pageStyles.vendorImg
                }
                source={
                    {uri: 'https://picsum.photos/700'}
                }/>
        </View>
    );
}

const FoodCategories = () => {
    return (
        <View style={{flexDirection: 'row', marginTop: 10}}>
            <Chip
                mode="outlined"
                disabled={true}
                style={styles.marginR}
            >
                Samosa
            </Chip>
            <Chip
                mode="outlined"
                disabled={true}
            >
                Dosa
            </Chip>
        </View>  
    );
}

const VendorAddress = () => {
    return (
        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop:10 }}>
            <Text>18250 N 25th Ave</Text>
            <Text style={{color: "#9a9a9a", fontSize: 12}}>2KM</Text>
        </View>    
    );
    
}

const PaymentMethod = () => {
    return (
        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop:10 }}>
            <View>
                <Paragraph style={{ color: "#9a9a9a" }}>Payment</Paragraph>
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between'}}>
                <Text style={{color: "#9a9a9a", fontSize: 12}}> Cash/Card</Text>

            </View> 
        </View>    
    ); 
}

const finishRating = () => {

}


const VendorListScreen = (props: any) => {
    return (
        <View style={{backgroundColor: "#eeeeee"}}>
            <ScrollView>
                <Card style={styles.card}
                    onPress={() => props.navigation.navigate('VendorDetail')}>
                <Card.Title title="Testing" left={LeftContent} />

                <Card.Content>
                    <View style={{flexDirection: 'column'}}>
                        <VendorImage />
                        <Paragraph style={pageStyles.address}> 
                            <Rating
                                type="star"
                                ratingCount={5}
                                imageSize={20}
                                onFinishRating={finishRating}
                                readonly={true}
                            />
                        </Paragraph>
                        <FoodCategories />
                        <VendorAddress/>
                        <PaymentMethod/>
                    </View>
                </Card.Content>

                {/* <Card.Actions>
                    <Button>Cancel</Button>
                    <Button>Ok</Button>
                </Card.Actions> */}
                </Card>

                <TouchableRipple
                    onPress={() => props.navigation.navigate('VendorDetail')}
                    rippleColor="rgba(0, 0, 0, .32)"
                >
                <Card style={styles.card}>
                <Card.Title title="Testing" left={LeftContent} />

                <Card.Content>
                    <View style={{flexDirection: 'column'}}>
                        <VendorImage />
                        <Paragraph style={pageStyles.address}> 
                            <Rating
                                type="star"
                                ratingCount={5}
                                imageSize={20}
                                onFinishRating={finishRating}
                                readonly={true}
                            />
                        </Paragraph>
                        <FoodCategories />
                        <VendorAddress/>
                        <PaymentMethod/>
                    </View>
                </Card.Content>

                    </Card>
                </TouchableRipple>    

                <Card style={styles.card}>
                <Card.Title title="Testing" left={LeftContent} />

                <Card.Content>
                    <View style={{flexDirection: 'column'}}>
                        <VendorImage />
                        <Paragraph style={pageStyles.address}> 
                            <Rating
                                type="star"
                                ratingCount={5}
                                imageSize={20}
                                onFinishRating={finishRating}
                                readonly={true}
                            />
                        </Paragraph>
                        <FoodCategories />
                        <VendorAddress/>
                        <PaymentMethod/>
                    </View>
                </Card.Content>

            </Card>
            </ScrollView>
        </View>
    );
}

export default VendorListScreen;

const pageStyles = StyleSheet.create({
    
    vendorImg: {
        width: 85,
        height: 85,
        borderRadius: 5,
        padding: 10

    },
    address: {
        paddingTop: 10,
        fontSize: 15
    }

});


