import * as React from 'react';
import { StyleSheet, ScrollView } from 'react-native';
import { Text, View } from '../components/Themed';
import { Card, Title, Paragraph, Chip, Button } from 'react-native-paper';
import { Rating } from 'react-native-ratings';
import { styles } from '../assets/css/CommanCss';
import { MaterialIcons } from '@expo/vector-icons';

//import { GoogleSignin, GoogleSigninButton } from '@react-native-community/google-signin';

/*const GoogleButton = () => {
    
const GoogleButton = () => {

    /*GoogleSigninButton.Size = {
        Icon: '48 x 48',
        Standard: '230 x 48',
        Wide: '312 x 48'
    };

    GoogleSigninButton.Color = {
        Auto: 'white',
        Light: 'blue',
        Dark: 'grey'
    };*/
//}
const googleSignIn = () => {
    // return (
    //     <GoogleSigninButton
    //         style={{ width: 192, height: 48 }}
    //         size={GoogleSigninButton.Size.Wide}
    //         color={GoogleSigninButton.Color.Dark}
    //         onPress={googleSignIn}
    //         disabled={false}
    //     />
    // );

}

const LoginScreen = (props: any) => {

    return (
        <View style={{ backgroundColor: "#eeeeee" }}>
            <ScrollView>

                <Card style={styles.card}>
                    <Card.Title title="Login"
                        titleStyle={{ fontSize: 17, color: "gray" }}
                    />
                    <Card.Content>
                        <View style={{ flexDirection: 'column', justifyContent: 'center' }}>
                            <View>
                                {/* <TextInput
                                label="Username/Phone number"
                                value=""
                                onChangeText={text => props.onChange}
                                mode="outlined"
                                    style={styles.marginT}
                                /> */}

                            </View>
                            <View>
                                {/* <TextInput
                                    label="Password"
                                    value=""
                                    onChangeText={text => props.onChange}
                                    mode="outlined"
                                    multiline={true}
                                    style={styles.marginT}
                                    secureTextEntry={true}
                
                                /> */}
                            </View>
                            <View>
                                <Button
                                    mode="contained"
                                    onPress={() => console.log('Pressed')}
                                    style={styles.marginT}
                                    dark={true}

                                >
                                    Login
                                </Button>
                            </View>

                        </View>
                    </Card.Content>
                </Card>

                <View style={{ flexDirection: 'column', justifyContent: 'center', marginTop: 20 }}>
                    <View>
                        {/* <GoogleButton /> */}
                    </View>
                </View>

            </ScrollView>
        </View>
    );

};

export default LoginScreen;


const pageStyles = StyleSheet.create({
    textIcon: {
        marginLeft: 10
    }

});