import * as React from 'react';
import {StyleSheet, ScrollView} from 'react-native';
import {Text, View} from '../components/Themed';
import { Card, Title, Paragraph, Chip} from 'react-native-paper';
import { Rating } from 'react-native-ratings';
import { SliderBox } from "react-native-image-slider-box";
import { styles } from '../assets/css/CommanCss';
import { MaterialIcons } from '@expo/vector-icons'; 
import { SimpleLineIcons, Octicons } from '@expo/vector-icons'; 


const vendorImageArr = ["https://picsum.photos/700", "https://picsum.photos/700"];

const FoodCategories = () => {
    return (
        <View style={{flexDirection: 'row', marginTop: 10}}>
            <Chip
                mode="outlined"
                disabled={true}
                style={styles.marginR}
            >
                Samosa
            </Chip>
            <Chip
                mode="outlined"
                disabled={true}
            >
                Dosa
            </Chip>
        </View>  
    );
}

const vendoreRating = () => { };

const menuIcon = (props: any) => <SimpleLineIcons name="notebook" size={18} color="black" />
const reviewIcon = (props: any) => <MaterialIcons name="rate-review" size={18} color="black" />

const VendorDetailScreen = (props: any) => {

    return (
        <View style={{ backgroundColor: "#eeeeee" }}>
            <ScrollView>
                <Card>
                    <SliderBox images={vendorImageArr} />
                    <Card.Content>
                        <FoodCategories />
                        <Text style={{ color: "green", marginTop: 10 }}>Open until 11PM</Text>
                        <View style={{ flexDirection: 'row', marginTop: 10}}>
                            <View><SimpleLineIcons name="phone" size={15} color="black" /></View>
                            <View><Text style={pageStyles.textIcon}>+91-8454038620</Text></View>
                        </View>
                    </Card.Content>
                </Card>

                <Card style={styles.card}>
                    <Card.Title title="Menu" left={menuIcon}
                        titleStyle={{fontSize: 15, marginLeft:-25, color: "gray"}}
                    />
                    <Card.Content>
                        <View style={{ flexDirection: 'row' }}>
                            <View><Octicons name="primitive-dot" size={12} color="gray" style={{ padding: 5 }} /></View>
                            <View><Text style={pageStyles.textIcon}>First Iteam</Text></View>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <View><Octicons name="primitive-dot" size={12} color="gray" style={{ padding: 5 }} /></View>
                            <View><Text style={pageStyles.textIcon}>First Iteam</Text></View>
                        </View>
                    </Card.Content>
                </Card>

                <Card style={styles.card}>
                    <Card.Title title="Start review" left={reviewIcon}
                        titleStyle={{fontSize: 15, marginLeft:-25, color: "gray"}}
                    />
                    <Card.Content>
                        <View style={{ flexDirection: 'column', justifyContent: 'center' }}>
                            <View>
                                <Rating
                                type="star"
                                ratingCount={5}
                                imageSize={20}
                                onFinishRating={vendoreRating}
                                readonly={false}
                            />
                            </View>
                            <View>
                                {/* <TextInput
                                label="Write Review"
                                value=""
                                onChangeText={text => vendoreRating()}
                                mode="outlined"
                                    multiline={true}
                                    style={styles.marginT}
                                /> */}
                            </View>
                            
                        </View>
                        
                    </Card.Content>
                </Card>

            </ScrollView>
        </View>    
    );
    
};

export default VendorDetailScreen;


const pageStyles = StyleSheet.create({
    textIcon: {
        marginLeft: 10
    }

});