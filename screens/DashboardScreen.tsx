import * as React from 'react';
import { StyleSheet, ScrollView, Image } from 'react-native';
import { Text, View } from '../components/Themed';
import { Card, Title, Paragraph, Chip, Button, Avatar } from 'react-native-paper';
import { Rating } from 'react-native-ratings';
import { styles } from '../assets/css/CommanCss';
import { MaterialIcons } from '@expo/vector-icons';
import { SimpleLineIcons, Octicons } from '@expo/vector-icons';


const DashboardScreen = (props: any) => {

    return (
        <View style={{ backgroundColor: "#eeeeee", flexDirection: 'row', justifyContent: 'center', marginTop: 20 }}>

            <View style={{ backgroundColor: "#eeeeee" }}>
                <Card style={styles.cardM}
                    onPress={() => console.log('Pressed')}
                >
                    {/* <Card.Title title="Card Title" subtitle="Card Subtitle" /> */}
                    <Card.Content>

                        <Image
                            source={{
                                uri: 'https://picsum.photos/700',
                            }}
                            style={{
                                width: 50,
                                height: 50,
                            }}
                        />
                        <Paragraph>List your Tadka</Paragraph>
                        {/* <Title >
                            
                        </Title> */}


                    </Card.Content>
                    {/* <Card.Cover source={{ uri: 'https://picsum.photos/700' }} /> */}
                    {/* <Card.Actions>
                        <Button>Cancel</Button>
                        <Button>Ok</Button>
                    </Card.Actions> */}
                </Card>
            </View>
            <View style={{ backgroundColor: "#eeeeee" }}>
                <Card style={styles.cardM}>
                    {/* <Card.Title title="Card Title" subtitle="Card Subtitle" /> */}
                    <Card.Content>
                        <Title>Card title</Title>
                        <Paragraph>Card content</Paragraph>
                    </Card.Content>
                </Card>
            </View>
            {/* <View style={{ backgroundColor: "#eeeeee" }}>
                <Button
                    mode="outlined"
                    icon="camera" onPress={() => console.log('Pressed')}>
                    Press me
                </Button>
            </View> */}
        </View>
    );

};

export default DashboardScreen;


const pageStyles = StyleSheet.create({
    textIcon: {
        marginLeft: 10
    }

});