import * as React from 'react';
import { StyleSheet, Image, ScrollView } from 'react-native';
import { Text, View } from '../components/Themed';
import { Avatar, Card, Paragraph, FAB, Divider, IconButton, Colors, TextInput, Button } from 'react-native-paper';
import { Rating } from 'react-native-ratings';
import { Chip } from 'react-native-paper';
import { styles } from '../assets/css/CommanCss';

const EditShopProfileScreen = (props: any) => {
    const [text, setText] = React.useState('');

    return (
        <View style={{ backgroundColor: "#eeeeee" }}>
            <ScrollView>
                <Card style={styles.card}
                    onPress={() => props.navigation.navigate('VendorDetail')}>
                    <Card.Title
                        title="Edit profile"
                    />
                    <Card.Content>
                        <View>
                            <TextInput
                                label="Shope Name"
                                value=""
                                multiline={true}
                                onChangeText={text => setText(text)}
                                mode="outlined"
                                style={styles.marginT}
                                numberOfLines={5}
                            />
                            <Button
                                icon="camera"
                                mode="outlined"
                                onPress={() => console.log('Pressed')}
                                style={{ padding: 5 }}

                            >
                                Logo
                            </Button>

                            <TextInput
                                label="Address"
                                value=""
                                multiline={true}
                                onChangeText={text => setText(text)}
                                mode="outlined"
                                style={styles.marginT}
                            />
                            <TextInput
                                label="City"
                                value=""
                                multiline={true}
                                onChangeText={text => setText(text)}
                                mode="outlined"
                                style={styles.marginT}
                            />
                            <TextInput
                                label="State"
                                value=""
                                multiline={true}
                                onChangeText={text => setText(text)}
                                mode="outlined"
                                style={styles.marginT}
                            />
                            <TextInput
                                label="Contact"
                                value=""
                                multiline={true}
                                onChangeText={text => setText(text)}
                                mode="outlined"
                                style={styles.marginT}
                            />
                            <TextInput
                                label="Email Address"
                                value=""
                                multiline={true}
                                onChangeText={text => setText(text)}
                                mode="outlined"
                                style={styles.marginT}
                                numberOfLines={5}
                            />

                            <TextInput
                                label="Payment type"
                                value=""
                                multiline={true}
                                onChangeText={text => setText(text)}
                                mode="outlined"
                                style={styles.marginT}
                                numberOfLines={5}
                            />

                        </View>
                        <View style={[{ flexDirection: 'row-reverse' }, styles.marginT]}>
                            <View style={{ padding: 5 }}>
                                <Button
                                    mode="contained"
                                    labelStyle={{ color: "white" }}
                                    onPress={() => console.log('Pressed')}
                                    icon="check"
                                >Update </Button>
                            </View>
                            <View style={{ padding: 5 }}>
                                <Button
                                    mode="outlined"
                                    onPress={() => console.log('Pressed')}
                                    icon="close"
                                >Reset </Button>
                            </View>
                        </View>
                    </Card.Content>
                </Card>
            </ScrollView>
        </View>
    );
}

export default EditShopProfileScreen;