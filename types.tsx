export type RootStackParamList = {
  Root: undefined;
  NotFound: undefined;
  VendorDetail: undefined;
  AddTadka: undefined;
  EditShopProfile: undefined;
};

export type BottomTabParamList = {
  Search?: undefined;
  Profile: undefined;
  Dashboard: undefined;
  TabTwo: undefined;
  ListTadka: undefined;
};

export type TabOneParamList = {
  TabOneScreen: undefined;
};

export type TabTwoParamList = {
  TabTwoScreen: undefined;
};

export type VendorParamList = {
  VendorListScreen: undefined;
};

export type LoginParamScreen = {
  LoginScreen: undefined;
};

export type DashboardParamScreen = {
  DashboardScreen: undefined;
};

export type ListedTadkaParamScreen = {
  ListedTadkaScreen: undefined;
};