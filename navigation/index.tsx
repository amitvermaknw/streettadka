import { NavigationContainer, DefaultTheme, DarkTheme } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import * as React from 'react';
import { ColorSchemeName } from 'react-native';
import { getFocusedRouteNameFromRoute } from '@react-navigation/native';
import NotFoundScreen from '../screens/NotFoundScreen';
import { RootStackParamList } from '../types';
import BottomTabNavigator from './BottomTabNavigator';
import LinkingConfiguration from './LinkingConfiguration';
import NavigationSearchInput from './NavigationSearch';
import VendorDetailScreen from '../screens/VendorsDetailScreen';
import AddTadkaScreen from '../screens/AddTadkaScreen';
import EditShopProfileScreen from '../screens/EditShopProfileScreen';

// "Fundamentals" guide: https://reactnavigation.org/docs/getting-started
export default function Navigation({ colorScheme }: { colorScheme: ColorSchemeName }) {
  return (
    <NavigationContainer
      linking={LinkingConfiguration}
      theme={colorScheme === 'dark' ? DarkTheme : DefaultTheme}>
      <RootNavigator />
    </NavigationContainer>
  );
}

function getHeaderTitle(route: any, props: any) {
  const routeName = getFocusedRouteNameFromRoute(route);
  switch (routeName) {
    case 'Search':
      return <NavigationSearchInput {...props} />;
    case 'Profile':
      return 'My profile';
    case 'ListTadka':
      return 'Your profile';
    case 'Dashboard':
      return 'Dashboard';
    default:
      return <NavigationSearchInput {...props} />;
  }
}

// Read more here: https://reactnavigation.org/docs/modal
const Stack = createStackNavigator<RootStackParamList>();

function RootNavigator() {
  return (
    <Stack.Navigator screenOptions={{ headerShown: true }}>
      <Stack.Screen name="Root" component={BottomTabNavigator}
        options={({ route, props }: any) => ({
          headerTitle: getHeaderTitle(route, props)
        })}
      />
      <Stack.Screen name="VendorDetail" component={VendorDetailScreen} options={{ title: 'Search' }} />
      <Stack.Screen name="AddTadka" component={AddTadkaScreen} options={{ title: 'Add Tadka' }} />
      <Stack.Screen name="EditShopProfile" component={EditShopProfileScreen} options={{ title: 'Edit Shop Profile' }} />
      <Stack.Screen name="NotFound" component={NotFoundScreen} options={{ title: 'Oops!' }} />
    </Stack.Navigator>
  );
}
