import React, { useEffect, useRef } from 'react';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
 
const NavigationSearchInput = (props: any) => {

  const ref: any = useRef();

  // useEffect(() => {
  //   console.log(JSON.stringify(props));
  // }, []);

  return (
      <GooglePlacesAutocomplete
        ref={ref}
        placeholder='Search'
        onPress={(data, details = null) => {
        // 'details' is provided when fetchDetails = true
        console.log(data, details);
        }}
        query={{
          key: 'AIzaSyAxVZqdY76i4GAId-K5x_HtDCYwWjaQIyg',
          language: 'en',
        }}
        currentLocation={true}  
        minLength={10}
        styles={{
          textInput: {
            height: 40,
            color: '#5d5d5d',
          },
          predefinedPlacesDescription: {
            color: '#1faadb',
          }
        }}
      />
    
  );
};
 
export default NavigationSearchInput;