import { FontAwesome } from '@expo/vector-icons';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import * as React from 'react';

import Colors from '../constants/Colors';
import useColorScheme from '../hooks/useColorScheme';
import TabOneScreen from '../screens/TabOneScreen';
import TabTwoScreen from '../screens/TabTwoScreen';
import VendorListScreen from '../screens/VendorListScreen';
import LoginScreen from '../screens/LoginScreen';
import DashboardScreen from '../screens/DashboardScreen';
import ListedTadkaScreen from '../screens/ListedTadkaScreen';
import {
  BottomTabParamList, TabOneParamList,
  TabTwoParamList, VendorParamList, LoginParamScreen,
  DashboardParamScreen, ListedTadkaParamScreen
} from '../types';

import NavigationSearchInput from './NavigationSearch';


const BottomTab = createBottomTabNavigator<BottomTabParamList>();

export default function BottomTabNavigator() {
  const colorScheme = useColorScheme();

  return (
    <BottomTab.Navigator
      initialRouteName="Search"
      tabBarOptions={{ activeTintColor: Colors[colorScheme].tint }}>
      <BottomTab.Screen
        name="Search"
        component={VendorListNavigator}
        options={{
          tabBarIcon: ({ color }) => <TabBarIcon name="search" color={color} />,
        }}
      />
      <BottomTab.Screen
        name="Dashboard"
        component={DashboardScreenNavigator}
        options={{
          tabBarIcon: ({ color }) => <TabBarIcon name="dashboard" color={color} />,
        }}
      />
      <BottomTab.Screen
        name="Profile"
        component={LoginScreenNavigator}
        options={{
          tabBarIcon: ({ color }) => <TabBarIcon name="user-circle-o" color={color} />,
        }}
      />
      <BottomTab.Screen
        name="ListTadka"
        component={ListedTadkaScreenNavigator}
        options={{
          tabBarIcon: ({ color }) => <TabBarIcon name="list-alt" color={color} />,
        }}
      />
    </BottomTab.Navigator>
  );
}


// https://icons.expo.fyi/
function TabBarIcon(props: { name: string; color: string }) {
  return <FontAwesome size={30} style={{ marginBottom: -3 }} {...props} />;
}

// Each tab has its own navigation stack, you can read more about this pattern here:
// https://reactnavigation.org/docs/tab-based-navigation#a-stack-navigator-for-each-tab
const TabOneStack = createStackNavigator<TabOneParamList>();

function TabOneNavigator() {
  return (
    <TabOneStack.Navigator>
      <TabOneStack.Screen
        name="TabOneScreen"
        component={TabOneScreen}
        options={{ headerTitle: 'Tab One Title' }}
      />
    </TabOneStack.Navigator>
  );
}

const TabTwoStack = createStackNavigator<TabTwoParamList>();

function TabTwoNavigator() {
  return (
    <TabTwoStack.Navigator>
      <TabTwoStack.Screen
        name="TabTwoScreen"
        component={TabTwoScreen}
        options={{ headerTitle: 'Tab Two Title' }}
      />
    </TabTwoStack.Navigator>
  );
}

const VendorList = createStackNavigator<VendorParamList>();

function VendorListNavigator() {
  return (
    <VendorList.Navigator>
      <VendorList.Screen
        name="VendorListScreen"
        component={VendorListScreen}
      />
    </VendorList.Navigator>
  )
}

const LoginStack = createStackNavigator<LoginParamScreen>();

function LoginScreenNavigator() {
  return (
    <LoginStack.Navigator>
      <LoginStack.Screen
        name="LoginScreen"
        component={LoginScreen}
        options={({ navigation, route }: any) => ({
          headerTitle: (props: any) => {
            navigation.setOptions({ title: "Login" })
          },
          params: 'login'
        })} //options={{ headerTitle: 'Tab Two Title' }}
      />
    </LoginStack.Navigator>
  )
}

const DashboardStack = createStackNavigator<DashboardParamScreen>();

function DashboardScreenNavigator() {
  return (
    <DashboardStack.Navigator>
      <DashboardStack.Screen
        name="DashboardScreen"
        component={DashboardScreen}
        options={{ headerTitle: 'Dashboard' }}
      />
    </DashboardStack.Navigator>
  )
}

const ListedTadkaStack = createStackNavigator<ListedTadkaParamScreen>();

function ListedTadkaScreenNavigator() {
  return (
    <ListedTadkaStack.Navigator>
      <ListedTadkaStack.Screen
        name="ListedTadkaScreen"
        component={ListedTadkaScreen}
        options={{ headerTitle: 'Listed Tadka' }}
      />
    </ListedTadkaStack.Navigator>
  )
}
