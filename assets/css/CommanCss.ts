import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold'
    },
    separator: {
        marginVertical: 30,
        height: 1,
        width: '80%'
    },
    card: {
        marginTop: 5,
        marginBottom: 5
    },
    cardM: {
        margin: 10
    },
    margin: {
        margin: 10
    },
    marginT: {
        marginTop: 10
    },
    marginB: {
        marginBottom: 10
    },
    marginL: {
        marginLeft: 10
    },
    marginR: {
        marginRight: 10
    },
    padding: {
        padding: 10
    }
});